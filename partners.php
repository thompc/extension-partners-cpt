<?php
/**
 * BiP Extension: Partners Post Type
 * Description: Create Partners post type and register ACF
 * @author: Chris Thompson
 * @url: https://bitbucket.org/thompc/extension-partners-cpt
 * @version: 1.1
 *
 */
//Register our post type
add_action( 'init', function() {
    $args = array(
        "labels" => array( "name" => "Partners", "singular_name" => "Partner" ),
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "partner", "with_front" => true ),
        "query_var" => true,
        'supports' => array('title', 'editor')
    );
    register_post_type( "partner", $args );
});

//Get ACF to search our /acf path for our .JSON file
add_filter('acf/settings/load_json', 'load_partners_acf');
function load_partners_acf( $paths ) {
    $paths[] = dirname(__FILE__).'/acf';
    return $paths;
}

// Image size
add_image_size( 'partner_logo', 200, 200, false);