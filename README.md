# Partners Post Type
This extension creates a custom post type 'Partners', and creates the relevant ACF fields.

##Dependencies
For use with Multiple Post Types Extension.

### Installation
1. CD to your `bip-extensions` directory
2. Clone the extension to your `bip-extensions` directory `git clone https://username@bitbucket.org/thompc/extension-partners-cpt`
3. Go to any page and add a new Multiple Post Types level
4. Select Partners from the drop down menu and select as many as needed